export function applyFilter(state) {

    let param1, param2, param3 ;
    state.filterKey1 === ''?param1='PG':param1 = state.filterKey1;
    param2 = state.filterKey2;
    param3 = state.filterKey3;

    let filteredListFirst = state.lists[param1];
    let filteredListSecond = [];
    if (param2 === '') {
        filteredListSecond = filteredListFirst;
    }else {
        filteredListFirst.forEach(list =>{
            switch (param2) {
                case 'hot':
                    if (list.is_hot === 1) {
                        filteredListSecond.push(list)
                    }
                    break;
                case 'new':
                    if (list.is_new === 1) {
                        filteredListSecond.push(list)
                    }
                    break;
                case 'mode':
                    if (list.mode === 2) {
                        filteredListSecond.push(list)
                    }
                    break;
            }
        });
    }
    let filteredListThird = [];
    if (param3 === '') {
        filteredListThird = filteredListSecond;
    }else {
        filteredListSecond.forEach(list =>{
            if (list.cn_name === param3) {
                filteredListThird.push(list);
            }
        })
    }
    state.filteredLists = filteredListThird
}
export function initPaginate(state) {
    state.totalPageNum = Math.ceil(state.filteredLists.length/state.itemPerPage);
    state.currentPage = [];
    let displayPageNum;
    state.filteredLists.length - (state.currentPageNum - 1) * state.itemPerPage > state.itemPerPage?displayPageNum = state.currentPageNum * state.itemPerPage:displayPageNum = state.filteredLists.length;
    for (let i = (state.currentPageNum - 1) * state.itemPerPage; i < displayPageNum; i++) {
        state.currentPage.push(state.filteredLists[i]);
    }
    state.totalPageNum - state.paginationStartPage > state.pageBtnNum? state.paginationEndPage = state.paginationStartPage + state.pageBtnNum -1: state.paginationEndPage = state.totalPageNum;
}
export function changePage(state, key) {
    switch (key) {
        case 'first':
            state.currentPageNum = 1;
            state.paginationStartPage = 1;
             break;
        case 'prev':
            if (state.currentPageNum > 1) {
                state.currentPageNum --;
                state.currentPageNum < state.paginationStartPage?state.paginationStartPage = state.paginationStartPage - state.pageBtnNum:1;
            }
            break;
        case 'next':
            if (state.currentPageNum < state.totalPageNum) {
                state.currentPageNum++;
                state.currentPageNum > state.paginationEndPage?state.paginationStartPage = state.paginationStartPage + state.pageBtnNum:1;
            }
            break;
        case 'last':
            state.currentPageNum = state.totalPageNum;
            state.totalPageNum%state.pageBtnNum === 0?state.paginationStartPage= state.totalPageNum - state.pageBtnNum + 1:state.paginationStartPage = Math.floor(state.totalPageNum / state.pageBtnNum) * state.pageBtnNum + 1;
            break;
    }
}
export function goToPage(state, pageNo) {
    state.currentPageNum = pageNo;
}