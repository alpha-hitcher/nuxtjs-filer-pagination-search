import * as Filters from '../helper/filter'
export const state = () => ({
    lists: [],
    filteredLists:[],
    filterKey1:'',
    filterKey2:'',
    filterKey3:'',
    currentPage:[],
    itemPerPage:12,
    pageBtnNum: 10,
    currentPageNum:1,
    totalPageNum:1,
    paginationStartPage : 1,
    paginationEndPage : 1
});
export const mutations = {
    setData (state, data){state.lists = data},
    setFilteredData(state, data){state.filteredLists = data},
    filter(state){ Filters.applyFilter(state)},
    setFilterKey(state, data) {state[data[0]] = data[1];if (data[0] === 'filterKey1') {state.filterKey2=''; state.filterKey3='';}else if (data[0] === 'filterKey2') {state.filterKey3 = '';} state.paginationStartPage =1;state.currentPageNum = 1; },
    initPaginate(state) { Filters.initPaginate(state);},
    changePage(state, key){ Filters.changePage(state, key)},
    goToPage(state, pageNo){Filters.goToPage(state, pageNo)}
};